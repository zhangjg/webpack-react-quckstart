/**
Component Contract List.
<a id="useage" />
`jsx
<ContractList list={array} addNew={function}/>
`
The item of the list should include fellow field.

| Field Name | Required | Note |
|-----------|----|------ |
| name | Yes| Name of the contract. |
| id  | No | Default as name. |
| onClick | Yes | Function callbacn when this contract be select |
@moudle ContractList

*/
const baseStyle={
  flex:1,
  display:"flex",
  flexDirection:'column',
  // alignItems:'center',
};
const style={
  root:{
    ...baseStyle,
    // width:'50%',
    borderStyle:'groove',
    borderRadiu:""
  },
  item:{
    display:'flex',
    justifyContent:'center',
    marginBottom:"0.5ex 0",
    borderStyle:'groove',
    background:'#0000ff5e'
  }
}
import React,{Component as C} from 'react';

function objToElement({name,id,onClick},index){
  return (<span
            style={style.item}
            key={index.toString()}
            title={id||name}
            onClick={onClick}
          >{name}
          </span>
        )
}

class ContracterList extends C {
  constructor(props){
    super(props);
    console.log('ContracerList')
  }
  render(){
    const contracters = this.props.list.map(objToElement);
    return (
      <div style={style.root}>
        <span
          style={style.item}
          title="add a new constracter"
          onClick={this.props.addNew}
        >
        +
        </span>
        {contracters}
      </div>
    )
  }
}

export default ContracterList;
