import React from 'react';
import Chat from './Chat.jsx'
const messages =[
  {
    from:"user1",
    time:Date.now()-20000,
    msg:"hello"
  },
  {
    time:Date.now()-10000,
    msg:'hello'
  },
];
function send(message){
  messages.push(message);
}

export default ()=> (<Chat user="user1" send={send} messages={messages} />)
