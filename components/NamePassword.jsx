/**
Module NamePassword

<a id="usage"/>

`jsx
<NamePassword onSubmit={fun} />
`

fun Should be a function as ({name,password})=>Promise.
@module NamePassword
*/
import React, {Component as C} from 'react';
const style={
  root:{
    display:"flex",
    flex:1,
    flexDirection:"column",
    justifyContent:"center",
  },
  errorTip:{
    background:"yellow",
    color:"red",
  },
}

/**
Return null or a string.
@arg {RegExp} regExp
@return null or string
@priave
*/
export function regexpToString(regExp){
  if( (typeof regExp) == 'string' ) {
    return regExp;
  }
  if(regExp == null)
  {
    return null
  }
  return regExp.source || regExp.toString()
}

class NamePassword extends C {
  /**
  The constructor of NamePassword.

  The props object:

  | Field name | Required | Explain |
  |-----------|:--------:|:-------:|
  | namePattern | No     | Regexp. Set pattern of name. |
  | nameTip     | No     | String. Set title of name. |
  | passwordPattern | No | Regexp. Set pattern of passwrod. |
  | passwordTip    | No | String. Set title of password. |
  | onSubmit | Yes | `function({name:string,password:string})=>Promise(bool)` |

  @arg {object} props
  */
  constructor(props){
    super(props);
    // console.log(props)
    this.state={
      name:'',
      password:'',
      error:null,
    }
  }
  /**
  Used to get the hanlder of `onChange` for 'name' and 'password'.
  @private
  @arg {string} name - Which state field will change, 'name' or 'password'.
  @return function(string);
  */
  _change(name){
    const changeHandler=(event)=>{
      this.setState({[name]:event.target.value});
    }
    return changeHandler;
  }

  /**

  Used to get the hanlder of 'onSubmit' for 'submit'.
  @private
  @return function(event);
  */
  _getSubmitHanddler(){
    const errHandler=(error)=>{
      this.setState({error});
    }
    const ok=()=>{
      this.setState({error:null});
    }
    const submitHanddler=(evt)=>{
      // console.log(evt);
      evt.preventDefault();
      this.props.onSubmit({
        name:this.state.name,
        password:this.state.password,
      }).then(ok,errHandler);
    }
    return submitHanddler;
  }

  render(){
    // console.log(this.state.namePattern);
    return (
      <form
        style={style.root}
        onSubmit={this._getSubmitHanddler()}
        >
        <div style={style.errorTip} >
          {this.state.error? this.state.error.message: '' }
        </div>
        <input type="text"
          placeholder="Name"
          title={this.props.nameTip}
          pattern={regexpToString(this.props.namePattern)}
          value={this.state.name}
          onChange={this._change('name')}
        />
        <input type="password"
          placeholder="password"
          title={this.props.passwordTip}
          pattern={regexpToString(this.props.passwordPattern)}
          value={this.state.password}
          onChange={this._change('password')}
        />
        <input type="submit" />
      </form>
    );
  }
}

export default NamePassword;
