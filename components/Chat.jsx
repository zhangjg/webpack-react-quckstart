/**
Chat Component.

<a id="usage"/>
# Usage Example:

`jsx
<Chat user={name} send={fun} messages={messagesArray} />
`

| Field Name | Type | Note |
|-----------|:-------:| :---------: |
| user | string | User's Name |
| send | function | Used to send message to user. |
| messages  | [object]| The |

@module Chat
*/
import {
  $,
  immediateScrollToButtom,
} from '../src/browser.js';

import React,{Component as C} from 'react';
const baseStyle={
  flex:1,
  display:'flex',
  flexDirection:'column',
  justifyContent:'center',
  // background:'red',
};
const messageStyle={
  flex:"none",
  borderRadius:'0.5ex',
  borderStyle:'groove',
  padding:'0 0.5ex',
  margin:'0.5ex 0',
};
const style={
  root:{
    ...baseStyle,
    borderRadius:'1.5ex',
    borderStyle:'groove',
    padding:"0 1.5ex",
    width:'90vw',
  },
  header:{
    ...baseStyle,
    flex:1,
    minHeight:'3ex',
    alignItems:'center',
    flexDirection:'row',
    justifyContent:'space-between',
    background:'yellow',
  },
  content:{
    ...baseStyle,
    flex:7,
    justifyContent:'flex-start',
    background:'#0000ff9c',
    overflowY:'scroll',
  },
  right:{
    ...baseStyle,
    ...messageStyle,
    alignSelf:'flex-end',
    alignItems:'flex-end',
    background:'yellow',
  },
  left:{
    ...baseStyle,
    ...messageStyle,
    alignSelf:'flex-start',
    alignItems:'flex-start',
  },
  footer:{
    ...baseStyle,
    flex:3,
    background:'green',
    padding:"0",
  },
  textarea:{
    flex:3,
    resize:'none',
  },
  toolbar:{
    ...baseStyle,
    flex:1,
    alignItems:'flex-end',
    justifyContent:'center',
  }
}

class Chat extends C {
  /**
  See [Usage]{@link #usage}
  */
  constructor(props){
    super(props);
    this.state={
      input:'',
    };
  }
  /**
  Use to get textarea change event handler function.
  @private
  */
  _change(){
    if(!this.changeHandler){
      const changeHandler=(event)=>{
        this.setState({input:event.target.value});
      }
      this.changeHandler = changeHandler;
    }
    return this.changeHandler;
  }
  /**
  Auto scroll the 'chat-content' to buttom.
  @private
  */
  _autoScroll(){
    // console.log(this.automScriollElement);
    if(!this.automScriollElement) {
      this.automScriollElement = $('#chat-content');
    }
    immediateScrollToButtom(this.automScriollElement);
  }
  /**
  Send the textarea value to user, and clean the textarea vlaue.
  @private
  */
  _send() {
    const clickHandler=event=>{
      let msg = this.state.input;
      let time = Date.now();
      this.props.send({msg,time});
      this.setState({input:''});
      //this._setMessage({msg,time});
      this._autoScroll();
    }
    return clickHandler;
  }

  // componentDidMount(){
  //
  // }
  // componentWillUnmount(){
  //   // this.props.user.off();
  // }
  render(){
    //console.log(this.state);
    let messages = this.props.messages.map(({from,msg,time})=>{
      let myStyle= (from != null ? style.left : style.right);
      return(
        <div key={time.toString()}
          style={myStyle}
        >
          <span>{Date(time)}</span>
          <span>{from||'Me'}</span>
          <span>{msg}</span>
        </div>
      )
    });
    return (
      <div style={style.root}>
        {/*header*/}
        <div style={style.header}>
         <span>{this.props.user}</span>
        </div>
        {/*Chat content*/}
        <div style={style.content} id="chat-content">
          {messages}
        </div>
        {/*Footer*/}
        <div style={style.footer}>
          <textarea
            style={style.textarea}
            value={this.state.input}
            onChange={this._change()}
          />
          <div style={style.toolbar}  >
            <input type="button" value='send'
               onClick={this._send()}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default Chat;
