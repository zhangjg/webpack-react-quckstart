/**
Component of Contracter.

<a id='usage' />
`jsx
<Contracter
  editable
  save={function}
  id={id}
  name={name}
  />
`
*/
import React, {Component as C} from 'react';

const baseStyle={
  flex:1,
  display:'flex',
  flexDirection:'column',
  justifyContent:'center',
  alignItems:'center',
}
const style={
  root:{
    ...baseStyle,
    alignItems:'narmal',
    borderStyle:'groove',
  },
  header:{
    // ...baseStyle
    borderBotton:"1ex solid solid",
  },
  item:{
    ...baseStyle,
    borderBottom:".5ex #b1b1bb solid",
    flexDirection:'row',
    justifyContent:'space-between',
  }
}
class Contracter extends C {
  /**
  The props Descript table:

  | Field | Required | Note |
  |-------|---------|-------|
  | editable | No | default is `false` |
  | save | Depend on `editable`. | Callack function if editable to save the change. |
  | id | No | String. Can not be change.|
  | name | No | String |
  | style | No | Object. Css in js. |
  */
  constructor(props) {
    super(props);
    this.state={
      name:this.props.name||"",
      id:this.props.id||"",
    };

  }
  _show(){
    return (
      <div style={this.rootStyle}>
        <div style={style.item} >
          <div>ID</div>
          <div>{this.props.id||""}</div>
        </div>
        <div style={style.item}>
          <div>Name</div>
          <div>{this.props.name||""}</div>
        </div>
      </div>
    )
  }
  _onChange(key){
    let funName=`${key}ChangeHandler`;
    if( !(funName in this) ){
      this[funName]=(event)=>{
        this.setState({
          [key]:event.target.value,
        });
      }
    }
    return this[funName];
  }
  _edit(){
    return (
      <div style={this.rootStyle}>
        <div style={style.item} >
          <div>ID</div>
          <input
            type="text"
            placeholder="ID"
            onChange={this._onChange('id')}
            value={this.state.id}
          />
        </div>
        <div style={style.item}>
          <div>Name</div>
          <input
              type="text"
              placeholder="Name"
              onChange={this._onChange('name')}
              value={this.state.name}
          />
        </div>
        <input
          type="button"
          value="Save"
          onClick={ ()=>this.props.save&&this.props.save(this.state) }
        />
      </div>
    )
  }
  render(){
    console.log('heelo')
    this.rootStyle={
      ...style.root,
      ...this.props.style,
    };
    if('editable' in this.props && this.props.editable !== false){
      return this._edit();
    }
    return this._show();
  }
}

export default Contracter;
