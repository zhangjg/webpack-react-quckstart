import {
  HashRouter as Router,
  Route,
  Switch,
}from "react-router-dom";
import React from 'react';

//Test Component
import ChartTest from './Chat.test.jsx';
import NamePasswordTest from './NamePassword.test.jsx';
import ContracterListTest from './ContracterList.test.jsx';
import ContracterTest from './Contracter.test.jsx';

//End Test Component

export default ()=>{
  return (
    <Router>
      <Switch >
        {/*Test Component*/}
        <Route path="/test/Chat" component={ChartTest} />
        <Route path="/test/NamePassword" component={NamePasswordTest} />
        <Route path="/test/Contracter" component={ContracterTest} />
        <Route path="/test/ContracterList" component={ContracterListTest} />
        {/*End test Component*/}
      </Switch>
    </Router>
  )
};
