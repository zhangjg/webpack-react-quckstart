/**
UI entery point.

All ui route should hanlde in `App()`.
@module App
*/

import React, {Component as C} from 'react';

import ReactDom from 'react-dom';


// import Test from './components/Test.jsx';
import Main from './Main.jsx';
/**
This is a React Commpent.

It can complex as much as need. So if need use `class` synatx
replace `function`.
*/
function App(){
  // return <Test/>
  return <Main />
}

/**
Start UI.

@arg {BrowserDOM} container - The react commpent insert in.
*/
function start(container){
  ReactDom.render(<App />,container);
}

const $ = document.querySelector.bind(document);

//Let's start.
start($("#app"));
