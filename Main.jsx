import React, {Component as C} from 'react';
import {
  HashRouter as Router,
  Route,
  Switch,
}from "react-router-dom";
import {
  $,
  localStorage,
  createUser,
  // login,
} from './src/browser.js';

import NamePassword from './components/NamePassword.jsx';

class Main extends C {
  constructor(props){
    super(props);
    this.state={
      user:null,
    }
  }
  componentDidCatch(error, info) {
    console.log(error,info);
  }
  createUser({name,password}) {
    return createUser(name,password).then(
      ()=>{
        this.setState({user:name});
      }
    )
  }
  render() {
    let user = this.state.user;
    let defaultRoute;
    console.log(user);
    if(user==null){
      defaultRoute = (
        <NamePassword
          namePattern={/\w{4,10}/}
          nameTip="4 ~ 10 characteres."
          passwordPatern={/\w{5,10}/}
          passwordTip="5 ~ 10 characteres."
          onSubmit={this.createUser.bind(this)}
        />
      )
    }else{
      defaultRoute = (
        <NamePassword
          namePattern={/\w{4,10}/}
          nameTip="4 ~ 10 characteres."
          passwordPatern={/\w{5,10}/}
          passwordTip="5 ~ 10 characteres."
          onSubmit={console.log}
        />
      )
    }
    return (
      <Router>
        <Switch>
          <Route render={()=>defaultRoute} />
        </Switch>
      </Router>
    )
  }
}

export default Main;
