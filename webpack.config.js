const path = require('path');

module.exports = {
  entry: {
    app: './App.jsx'
  },
  // target: 'electron',
  module: {
    rules: [
      {
        test: /\.jsx$/,
        use: {
          loader:'babel-loader',
          options: {
            presets: ['@babel/preset-env','@babel/preset-react',]
          },
        },
      },
    ],
  },
  mode:'development',
  devtool: 'inline-source-map',
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'static'),
  }
};
