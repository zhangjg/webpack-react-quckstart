/**
Util function use in browser.
@module browser
*/

/**
Query document element use css selector.
@arg {string} selector - Css selector.
@return BrowserDOM or null.
*/
const $ = document.querySelector.bind(document);

/**
Similar as {@link module:browser~$}, but return a array.
@arg {string} selector - Css selector.
@return [BrowserDOM] or []
*/
const $$ = document.querySelectorAll.bind(document);

/**
Scroll to the element buttom.
@arg {BrowserDOM} ele - The element you want to scroll.
@return null.
*/
function scrollToButtom(ele){
  ele.scrollTop = ele.scrollHeight;
}

/**
Similar as {@link module:browser~scrollToButtom}, but scroll in next event loop.
@arg {BrowserDOM} ele - The element you want to scroll.
@return null.
*/
function immediateScrollToButtom(ele){
  setTimeout(scrollToButtom.bind(null,ele),0);
}

import Gun from 'gun';
import Sea from 'gun/sea.js';
const localStorage=window.localStorage;

let gun = null;
let user = null;
function getGun(){
  if(gun == null){
    gun = new Gun('http://localhost:8080/gun');
  }
  return gun;
}

function getUser(){
  if(user == null){
    user = getGun().user();
  }
  return user;
}

function createUser(name,password) {
  function userCreatePromise(resolve,reject){
    console.log('hello')
    const user = getUser();
    console.log('hello')
    user.create(
      name,
      password,
      ack=>{
        console.log(ack);
        if(ack.ok == 0 ){
          console.log(ack);
          resolve(ack.pub);
          return;
        }
        reject(Error(`${name} ready created.`));
      }
    )
  }
  return new Promise(userCreatePromise);
}

function login(name,password){
  function loginPromise(resolve,reject){
    const user = getUser();
    user.auth(
      name,
      password,
      ack=>{
        console.log(ack);
        if(ack.ok == 0){
          let {ok:_,...o} = ack;
          return reslove(o);
        }
        return reject(ack);
      }
    );
  }
  return new Prmise(loginPromise);
}

export {
  $,
  $$,
  scrollToButtom,
  immediateScrollToButtom,
  localStorage,
  getGun,
  getUser,
  createUser,
  login,
};
