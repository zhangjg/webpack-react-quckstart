const KOA = require('koa');
const fs = require('fs');
const path = require('path');
const Gun = require('gun');
const http = require('http');
Gun.on('create',function(db){
  this.to.next(db);
  db.on(
    'get',
    function(request){
      this.to.next(request);
      console.log(request);
    }
  );
  db.on(
    'put',
    function(request){
      this.to.next(request);
      console.log(request);
    }
  );
});
const MIME=Object.freeze({
  html:"text/html",
  js:"application/x-javascript",
});
function getReadStreamOf(fileName,root){
  function createReadStream(resolve,reject) {
    let readStream = fs.createReadStream(`${root}${fileName}`);
    readStream.on('error',reject);
    resolve(readStream);
  }
  return new Promise(createReadStream);
}

function staticFile(root){
  return async function staticHanlder(ctx,next){
    let file = null;
    switch(true){
      case (ctx.url == '/'):
      case (ctx.url == '/index.html'):
      file= await getReadStreamOf(`/index.html`,root);
      ctx.type = MIME['html'];
      break;
      case (ctx.url = '/app.js'):
      file = await getReadStreamOf('/app.js',root);
      break;
      default:
      try{
        file = await getReadStreamOf(ctx.url,root);
        let ext = path.extname(ctx.url).split('.')[1];
        ctx.type = MIME[ext];
      }catch(err) {
        console.log(err);
        ctx.body = err;
        ctx.status = 404;
        return;
      }
    }
    ctx.body = file;
  }
}

function gunMiddlewale(){
  return async function handlerGun(ctx,next){
    if(Gun.serve(ctx.req,ctx.res)){
      return;
    }
    await next();
  }
}

function main(port){
  const app = new KOA();
  app.use(gunMiddlewale());
  app.use(staticFile(`${__dirname}/static`));
  let server = http.createServer(app.callback());
  const gun=Gun({web:server});
  server.listen(
    port,
    function listenCallBack(){
      console.log(`listen on ${port}`);
    }
  )
}

if(require.main === module){
  main(8080);
}
